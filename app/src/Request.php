<?php

namespace App;

class Request
{
    private $pathParametes = [];

    public function __construct(string $path, array $queryParameters)
    {
        $this->path = $path;
        $this->queryParameters = $queryParameters;
    }

    public static function fromGlobals()
    {
        $uri = $_SERVER['REQUEST_URI'];
        $index = strpos($uri, '?');
        if ($index === false) {
            $path = $uri;
        } else {
            $path = substr($uri, 0, $index);
        }

        return new self($path, $_GET);
    }

    public function getPath()
    {
        return $this->path;
    }

    public function hasQueryParam(string $name)
    {
        return isset($this->queryParameters[$name]);
    }

    public function getQueryParam(string $name, $default = null)
    {
        return $this->queryParameters[$name] ?? $default;
    }

    public function getPathParameters()
    {
        return $this->pathParametes;
    }

    /**
     * @param array $params
     */
    public function setParameters($params)
    {
        $this->pathParametes = $params;
    }

    public function getParameter($name, $default = null)
    {
        return $this->pathParametes[$name] ?? $default;
    }
}
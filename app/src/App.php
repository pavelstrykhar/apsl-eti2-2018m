<?php

namespace App;

use App\Controllers\ControllerInterface;

class App
{
    /**
     * @var string
     */
    private $page;

    private $layout;

    private $request;

    /**
     * Uruchamia aplikację
     */
    public function run(): void
    {
        $this->request = Request::fromGlobals();

        $router = $this->getRouter();
        $matchedRoute = $router->match($this->request);

        if ($matchedRoute instanceof ControllerInterface) {
            $response = $matchedRoute($this->request);
            foreach ($response->getHeaders() as $header) {
                header($header);
            }

            echo $response->getBody();
        } else {
            $this->layout = new Layout($this->request, $matchedRoute, 'default');
            $this->layout->render();
        }
    }

    /**
     * @return string
     */
    public function getPage(): string
    {
        return $this->page;
    }

    /**
     * @return Router
     * @throws \Exception
     */
    public function getRouter()
    {
        return ServiceContainer::getInstance()->get('router');
    }
}